package menuClasses;

import java.util.ArrayList;

public class OperateListsMenu extends Menu {
	 
	private OperateListsMenu(boolean alteringList) { 
		super(); 
		String title; 
		ArrayList<Option> options = new ArrayList<Option>();
		if(!alteringList) {
			title = "Operate on Lists"; 
			options.add(new Option("Show all Lists", new ShowListsAction())); 
			options.add(new Option("Show Content of a List", new ShowListAction())); 
			options.add(new Option("Add a New Value to a List", new AddToListAction())); 
			options.add(new Option("Delete Position from a List", new DeleteFromListAction())); 
			options.add(new Option("Get Statistics from Lists", new ListStatisticsAction())); 
			options.add(Option.EXIT); 
	
			super.InitializeMenu(title, options); 
		}
		else if(alteringList) {
			title = "Alter List Menu"; 
			options.add(new Option("Add a New Value to a list", new ShowListsAction())); 
			options.add(new Option("Delete a Position form a List", new ShowListAction())); 
			options.add(new Option("Show Content of a List", new AddToListAction())); 
			options.add(Option.EXIT); 
	
			super.InitializeMenu(title, options); 
		}

	}
	
	public static OperateListsMenu getOperateListsMenu(boolean alteringList) { 
		return new OperateListsMenu(alteringList);
	}
}
