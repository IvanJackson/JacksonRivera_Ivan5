package menuClasses;

import dataManager.DMComponent;

public class AlterListContentMenu implements Action {

	@Override
	public void execute(Object arg) {
		DMComponent dm = (DMComponent) arg; 
		dm.getMenuStack().push(OperateListsMenu.getOperateListsMenu(true)); 
	}

}
